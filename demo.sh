#!/bin/sh
set -e

export LIBVIRT_DEFAULT_URI=qemu:///system


echo "# 1st, Let's verify that the Rhyzome server is running.\n" | lolcat -a

sleep 3

echo "shruthi@demo~$ ps aux | grep rhyzome | grep -v grep"
sleep 1.5 
echo "Execute!" | lolcat -a
ps aux | grep rhyzome | grep -v grep

sleep 2

echo "\n# The Rhyzome daemon appears to be running...\n" | lolcat -a

sleep 3

echo "# 2nd, Let's build a unikernel.\n" | lolcat -a

echo "shruthi@demo~$ git clone https://github.com/mirage/mirage-skeleton"
sleep 1.5 
echo "Execute!" | lolcat -a
git clone https://github.com/mirage/mirage-skeleton || true

echo "\nshruthi@demo~$ cd mirage-skeleton/applications/static_website_tls"
sleep 1.5 
echo "Execute!" | lolcat -a
cd mirage-skeleton/applications/static_website_tls


echo "\nshruthi@demo~$ mirage configure -t virtio"
sleep 1.5 
echo "Execute!" | lolcat -a
mirage configure -t virtio

echo "\nshruthi@demo~$ make depend"
sleep 1.5 
echo "Execute!" | lolcat -a
make depend

echo "\nshruthi@demo~$ make"
sleep 1.5 
echo "Execute!" | lolcat -a
make

echo "\nshruthi@demo~$ ls *.virtio"
sleep 1.5 
echo "Execute!" | lolcat -a
ls *.virtio

sleep 3


echo "\n# 3rd, Now we can provision our new unikernel via Rhyzome.\n"| lolcat -a

sleep 3

echo "shruthi@demo~$ rhyzomectl --server http://127.0.0.1:8080\n                          instance create\n                          --name https-unikernel\n                          --kernel $PWD/https.virtio\n                          --kernel-parameters "--ipv4-gateway 192.168.100.1 --ipv4 192.168.100.43/24""
sleep 1.5 
echo "Execute!" | lolcat -a

rhyzomectl --server http://127.0.0.1:8080 \
	   instance create \
	   --name https-unikernel \
	   --kernel $PWD/https.virtio \
	   --kernel-parameters "--ipv4-gateway 192.168.100.1 --ipv4 192.168.100.43/24"

sleep 3

echo "\n# We can verify with a standard libvirt client that the unkernel is running:" | lolcat -a

sleep 3

echo "\nshruthi@demo~$ virsh list"
sleep 1.5 
echo "Execute!" | lolcat -a
virsh list

sleep 3

echo "\n# Finally we can connect to the unikernel console, and in another shell we will attempt to communicate with the unikernel over the network" | lolcat -a

sleep 4

tmux new-session -s test 'echo "# We will connect to the console using the libvirt client.\n" | lolcat -a ; sleep 4 ; echo "shruthi@demo~$ virsh console https-unikernel" ; sleep 1.5 ; echo "Execute!" | lolcat -a ; LIBVIRT_DEFAULT_URI=qemu:///system virsh console https-unikernel' \; split-window -h 'sleep 7 ; echo "# Now we will attempt to talk to the unikernel over the network by using \`curl\`..." |lolcat -a ; sleep 3 ; echo "shruthi@demo~$ curl -k -s https://192.168.100.43:4433 | lynx -stdin -dump" ; sleep 0.5 ; curl -k -s https://192.168.100.43:4433 | lynx -stdin -dump ; sleep 2.5 ; echo "# Success! Success! Success!" | lolcat -a ; sleep 30' \; attach
